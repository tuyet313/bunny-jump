import Phaser from './lib/phaser.js'
import Game from './scenes/Game.js'
import GameOver from './scenes/GameOver.js'

// Phaser.Game takes a configuration object
export default new Phaser.Game ({
    type: Phaser.AUTO,  // Phaser will decide to use Canvas or WebGL mode depending on the browser and device
    width: 480,
    height: 640,
    scene: [Game, GameOver],

    // Add Arcade Physics
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {
                y: 200
            },
            // debug: true
        }
    },
})

console.dir(Phaser)