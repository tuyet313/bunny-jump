import Phaser from '../lib/phaser.js';

export default class GameOver extends Phaser.Scene
{
    constructor()
    {
        super('game-over')
    }

    create()
    {
        // use the ScaleManager to get the width and height
        const width = this.scale.width
        const height = this.scale.height

        // add a simple “Game Over” message
        this.add.text(width * 0.5, height * 0.5, 'Game Over', {
            fontSize: 48
        })
            .setOrigin(0.5) // centered vertically and horizontally

        // use the InputManager to listen for when the space key is pressed and then start the Game Scene
        this.input.keyboard.once('keydown-SPACE', () => {
            this.scene.start('game')
        })
    }
}