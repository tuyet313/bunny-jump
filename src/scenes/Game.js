import Phaser from '../lib/phaser.js';
import Carrot from '../game/Carrot.js';

export default class Game extends Phaser.Scene
{
    /** @type {Phaser.Physics.Arcade.StaticGroup} */
    platforms

    /** @type {Phaser.Types.Input.Keyboard.CursorKeys} */
    cursors

    /** @type {Phaser.Physics.Arcade.Group} */
    carrots 

    carrotsCollected = 0

    constructor()
    {
        super('game')
    }

    init()
    {
        this.carrotsCollected = 0
    }

    preload()
    {
        // load the background image
        this.load.image('background', 'assets/bg_layer1.png')

        // load the platform image
        this.load.image('platform', 'assets/ground_grass.png')
    
        // load the bunny image
        this.load.image('bunny-stand', 'assets/bunny1_stand.png')
        this.load.image('bunny-jump', 'assets/bunny1_jump.png')

        // load the carrot image
        this.load.image('carrot', 'assets/carrot.png')

        // load the audio
        this.load.audio('jump', 'assets/sfx/phaseJump1.ogg')     

        // set game to center
        this.scale.autoCenter = 1
    }

    create()
    {
        // add a background image in the middle
        this.add.image(240, 320, 'background')
            .setScrollFactor(1, 0)  // Keep the Background from Scrolling

        // create the group
        this.platforms = this.physics.add.staticGroup()

        // then create 5 platforms from the group
        for (let i = 0; i < 5; ++i)
        {
            const x = Phaser.Math.Between(80, 400)
            const y = 150 * i

            /** @type {Phaser.Physics.Arcade.Sprite} */
            const platform = this.platforms.create(x, y, 'platform')
            platform.scale = 0.5

            /** @type {Phaser.Physics.Arcade.StaticBody} */
            const body = platform.body
            body.updateFromGameObject()
        }

        // create a bunny sprite
        this.player = this.physics.add.sprite(240, 320, 'bunny-stand')
            .setScale(0.5)

        // Adding Collisions
        this.physics.add.collider(this.platforms, this.player)

        this.player.body.checkCollision.up = false
        this.player.body.checkCollision.left = false
        this.player.body.checkCollision.right = false

        // Follow that Rabbit
        this.cameras.main.startFollow(this.player)

        // set the horizontal dead zone to 1.5x game width
        this.cameras.main.setDeadzone(this.scale.width * 1.5)

        // Add player input via the keyboard
        this.cursors = this.input.keyboard.createCursorKeys()

        // create a carrot
        // const carrot = new Carrot(this, 240, 320, 'carrot')
        // this.add.existing(carrot)

        // Create a Group of Physics-Enabled Carrots
        this.carrots = this.physics.add.group({
            classType: Carrot
        })

        //this.carrot.get(240, 320, 'carrot')

        this.physics.add.collider(this.platforms, this.carrots)

        // formatted this way to make it easier to read
        this.physics.add.overlap(
            this.player, 
            this.carrots,
            this.handleCollectCarrot,   // called on overlap
            undefined,
            this
        )

        const style = { color: '#000', fontSize: 24 }
        this.carrotsCollectedText = this.add.text(240, 10, 'Carrots: 0', style)
            .setScrollFactor(0) // disable scrolling
            .setOrigin(0.5, 0)  // keep the text top-centered
    }

    update()
    {
        // Reuse and Recycle
        this.platforms.children.iterate(child => {
            /** @type {Phaser.Physics.Arcade.Sprite} */
            const platform = child

            const scrollY = this.cameras.main.scrollY
            if(platform.y >= scrollY + 700)
            {
                platform.y = scrollY - Phaser.Math.Between(50, 100)
                platform.body.updateFromGameObject()

                // create a carrot above the platform being reused
                this.addCarrotAbove(platform)
            }
        })

        // find out from Arcade Physics if the player's physics body is touching something below it
        const touchingDown = this.player.body.touching.down

        if (touchingDown)
        {
            // this makes the bunny jump straight up
            this.player.setVelocityY(-300)

            // switch to jump texture
            this.player.setTexture('bunny-jump')

            this.sound.play('jump')
        }

        // check that the y velocity is great than 0 
        // to know that the bunny is falling and not rising
        const vy = this.player.body.velocity.y
        if (vy > 0 && this.player.texture.key !== 'bunny-stand')
        {
            // switch back to jump when falling
            this.player.setTexture('bunny-stand')
        }

        // left and right input logic
        if (this.cursors.left.isDown && !touchingDown)
        {
            this.player.setVelocityX(-200)
        }
        else if (this.cursors.right.isDown && !touchingDown)
        {
            this.player.setVelocityX(200)
        }
        else
        {
            // stop movement if not left or right
            this.player.setVelocityX(0)
        }

        this.horizontalWrap(this.player)

        // Get the bottom most platform
        const bottomPlatform = this.findBottomMostPlatform()
        // check if that the player is more than 200 pixels past the bottomPlatform
        if (this.player.y > bottomPlatform.y + 200)
        {
            // console.log('game over')

            // fall past the bottom most platform
            this.scene.start('game-over')
        }
    }

    /** 
     * @param {Phaser.GameObjects.Sprite} sprite
     */
    horizontalWrap(sprite)
    {
        const halfWidth = sprite.displayWidth * 0.5
        const gameWidth = this.scale.width
        if (sprite.x < -halfWidth)
            sprite.x = gameWidth + halfWidth
        else if (sprite.x > gameWidth + halfWidth)
            sprite.x = -halfWidth
    }

    /** 
     * @param {Phaser.GameObjects.Sprite} sprite
     */
    addCarrotAbove(sprite)
    {
        const y = sprite.y - sprite.displayHeight

        /** @type {Phaser.Physics.Arcade.Sprite} */
        const carrot = this.carrots.get(sprite.x, y, 'carrot')

        // set active and visible
        carrot.setActive(true)
        carrot.setVisible(true)

        this.add.existing(carrot)

        // update the physics body size
        carrot.body.setSize(carrot.width, carrot.height)

        // make sure body is enabed in the physics world
        this.physics.world.enable(carrot)

        return carrot
    }

    /**
    * @param {Phaser.Physics.Arcade.Sprite} player
    * @param {Carrot} carrot
    */
    handleCollectCarrot(player, carrot)
    {
        // hide from display
        this.carrots.killAndHide(carrot)

        // disable from physics world
        this.physics.world.disableBody(carrot.body)

        // increment by 1
        this.carrotsCollected++

        // create new text value and set it
        const value = `Carrots: ${this.carrotsCollected}`
        this.carrotsCollectedText.text = value
    }

    findBottomMostPlatform()
    {
        // Get all the platforms as an Array
        const platforms = this.platforms.getChildren()
        // Pck the first one in the Array as the current bottom most platform.
        let bottomPlatform = platforms[0]

        // Iterate over the Array and compare each platform against the current bottomPlatform
        for (let i = 1; i < platforms.length; ++i)
        {
            const platform = platforms[i]

            if (platform.y < bottomPlatform.y)
            {
                continue
            }

            bottomPlatform = platform
        }

        // Iteracted over the entire Array, the last platform stored in bottomPlatform is the bottom most platform and gets returned
        return bottomPlatform
    }
}